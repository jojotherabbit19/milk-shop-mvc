﻿using AppService.IRepository;
using milk_shop_MVC.Repository;

public class UnitOfWork : IUnitOfWork
{
    private readonly AppDbContext _appDbContext;
    public UnitOfWork(AppDbContext appDbContext)
    {
        _appDbContext = appDbContext;
    }
    public IProductRepository ProductRepository => new ProductRepository(_appDbContext);

    public IOrderRepository OrderRepository => new OrderRepository(_appDbContext);
    public IOrderDetailRepository OrderDetailRepository => new OrderDetailRepository(_appDbContext);

    public IUserRepository UserRepository => new UserRepository(_appDbContext);

    public ICategoryRepository CategoryRepository => new CategoryRepository(_appDbContext);

    public IBrandRepository BrandRepository => new BrandRepository(_appDbContext);

    public ICartRepository CartRepository => new CartRepository(_appDbContext);

    public async Task<int> SaveChangeAsync()
    {
        return await _appDbContext.SaveChangesAsync();
    }
}