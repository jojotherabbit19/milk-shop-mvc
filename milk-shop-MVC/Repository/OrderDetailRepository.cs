﻿using AppService.IRepository;

namespace milk_shop_MVC.Repository
{
    public class OrderDetailRepository : GenericRepository<OrderProduct>, IOrderDetailRepository
    {
        public OrderDetailRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
