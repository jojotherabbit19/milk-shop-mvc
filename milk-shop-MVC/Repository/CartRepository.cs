﻿using AppService.IRepository;
using Microsoft.EntityFrameworkCore;

namespace milk_shop_MVC.Repository
{
    public class CartRepository : GenericRepository<Cart>, ICartRepository
    {
        public CartRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<Cart>> GetCartDetail(int id) => await _dbSet.Where(x => x.User.Id == id && x.IsBuyed == false).Include(x => x.Product).ToListAsync();
    }
}
