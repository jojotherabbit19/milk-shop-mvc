﻿using AppService.IRepository;
using Microsoft.EntityFrameworkCore;

namespace milk_shop_MVC.Repository
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Order?> GetOrderDetail(int id)
        {
            var result = await _dbSet.Include(x => x.OrderProducts).Where(x => x.Id == id).FirstOrDefaultAsync();
            return result;
        }

        public async Task<List<Order>?> GetOrderDetailList()
        {
            var result = await _dbSet.Include(x => x.OrderProducts).ToListAsync();
            return result;
        }
    }
}
