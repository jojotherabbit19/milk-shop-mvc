﻿using AppService.IRepository;
using Microsoft.EntityFrameworkCore;

namespace milk_shop_MVC.Repository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<bool> IsExist(string username)
        {
            var user = await _dbSet.FirstOrDefaultAsync(x => x.UserName == username);
            if (user is null)
            {
                return true;
            }
            return false;
        }

        public async Task<User> Login(string username, string password)
        {
            var user = await _dbSet.FirstOrDefaultAsync(x => x.UserName == username && x.PasswordHash == password);
            if (user is null)
            {
                throw new Exception("Not found this User");
            }
            return user;
        }
    }
}
