﻿using AppService.IRepository;

namespace milk_shop_MVC.Repository
{
    public class BrandRepository : GenericRepository<Brand>, IBrandRepository
    {
        public BrandRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
