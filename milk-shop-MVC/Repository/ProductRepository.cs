﻿using Microsoft.EntityFrameworkCore;

public class ProductRepository : GenericRepository<Product>, IProductRepository
{
    public ProductRepository(AppDbContext dbContext) : base(dbContext)
    {
    }

    public async Task<Product> GetDetail(int id)
    {
        return await _dbSet.Include(x => x.Brand).Include(x => x.Category).FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<List<Product>> GetProductByBrand(int id)
    {
        return await _dbSet.Where(x => x.BrandId == id).ToListAsync();
    }

    public async Task<List<Product>> GetProductByCategory(int id)
    {
        return await _dbSet.Where(x => x.CategoryId == id).ToListAsync();
    }

    public async Task<List<Product>> GetTop6ByAsc()
    {
        return await _dbSet.OrderBy(x => x.Price).Take(6).ToListAsync();
    }
}