﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;
using milk_shop_MVC.Models;
using System.Diagnostics;

namespace milk_shop_MVC.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IProductService productService, IBrandService brandService, ICartDetailService cartDetailService, ICategoryService categoryService, IOrderService orderService, IOrderDetailService orderDetailService, IUserService userService) : base(productService, brandService, cartDetailService, categoryService, orderService, orderDetailService, userService)
        {
        }

        public async Task<IActionResult> Index()
        {
            var products = await _productService.GetTop6();
            return View(products);
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Cart()
        {
            return View();
        }
        public IActionResult ProductDetail()
        {
            return View();
        }
        public IActionResult Checkout()
        {
            return View();
        }
        public IActionResult Shop()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        public IActionResult Register()
        {
            return View();
        }
    }
}