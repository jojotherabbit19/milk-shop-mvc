﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;

namespace milk_shop_MVC.Controllers
{
    public class OrderController : BaseController
    {
        public OrderController(IProductService productService, IBrandService brandService, ICartDetailService cartDetailService, ICategoryService categoryService, IOrderService orderService, IOrderDetailService orderDetailService, IUserService userService) : base(productService, brandService, cartDetailService, categoryService, orderService, orderDetailService, userService)
        {
        }
        public async Task<IActionResult> Index()
        {
            int id = int.Parse(HttpContext.Session.GetString("userId"));
            if (id == 0)
            {
                throw new Exception("User not login");
            }
            var result = await _cartDetailService.GetCart(id);
            if (result is null)
            {
                ViewBag.Message = "Your Cart is empty";
            }
            return View(result);
        }
        [HttpPost]
        public async Task<IActionResult> CreateOrder(decimal total, List<KeyValuePair<int, int>> listQty)
        {
            var order = new Order
            {
                UserId = int.Parse(HttpContext.Session.GetString("userId")),
                OrderDate = DateTime.UtcNow,
                Total = total
            };
            await _orderService.CreateOrder(order);
            foreach (var item in listQty)
            {
                var orderProduct = new OrderProduct
                {
                    Order = order,
                    ProductId = item.Key,
                    Quantity = item.Value
                };
                await _orderDetailService.CreateOrderProduct(orderProduct);
            }
            var cart = await _cartDetailService.GetCart(int.Parse(HttpContext.Session.GetString("userId")));
            foreach (var item in cart)
            {
                item.IsBuyed = true;
                await _cartDetailService.UpdateCartProduct(item);
            }
            
            return RedirectToAction("Index", "Product");
        }


    }
}
