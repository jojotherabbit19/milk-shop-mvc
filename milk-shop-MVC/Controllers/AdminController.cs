﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;

namespace milk_shop_MVC.Controllers
{
    public class AdminController : BaseController
    {
        public AdminController(IProductService productService, IBrandService brandService, ICartDetailService cartDetailService, ICategoryService categoryService, IOrderService orderService, IOrderDetailService orderDetailService, IUserService userService) : base(productService, brandService, cartDetailService, categoryService, orderService, orderDetailService, userService)
        {
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
