﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;

namespace milk_shop_MVC.Controllers
{
    public class CartController : BaseController
    {
        public CartController(IProductService productService, IBrandService brandService, ICartDetailService cartDetailService, ICategoryService categoryService, IOrderService orderService, IOrderDetailService orderDetailService, IUserService userService) : base(productService, brandService, cartDetailService, categoryService, orderService, orderDetailService, userService)
        {
        }
        public async Task<IActionResult> Index()
        {
            int id = int.Parse(HttpContext.Session.GetString("userId"));
            if (id == 0)
            {
                throw new Exception("User not login");
            }
            var result = await _cartDetailService.GetCart(id);
            if (result is null)
            {
                ViewBag.Message = "Your Cart is empty";
            }
            return View(result);
        }
        public async Task<IActionResult> AddProductToCart(int Id, int qty)
        {
            var cart = new Cart
            {
                UserId = int.Parse(HttpContext.Session.GetString("userId")),
                IsBuyed = false,
                ProductId = Id,
                Quantity = qty
            };
            await _cartDetailService.CreateCartProduct(cart);
            return RedirectToAction("Index", "Product");
        }
        [HttpPost]
        public async Task<IActionResult> UpdateQty(int id, int qty)
        {
            var cart = await _cartDetailService.GetCartById(id);
            cart.Quantity = qty;
            await _cartDetailService.UpdateCartProduct(cart);
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Delete(int id)
        {
            var cart = await _cartDetailService.DeleteCartProduct(id);
            return RedirectToAction("Index");
        }
    }
}
