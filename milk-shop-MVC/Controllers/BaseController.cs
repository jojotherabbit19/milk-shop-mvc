﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;
using milk_shop_MVC.Models;
using System.Diagnostics;

namespace milk_shop_MVC.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IProductService _productService;
        protected readonly IBrandService _brandService;
        protected readonly ICartDetailService _cartDetailService;
        protected readonly ICategoryService _categoryService;
        protected readonly IOrderService _orderService;
        protected readonly IOrderDetailService _orderDetailService;
        protected readonly IUserService _userService;

        public BaseController(IProductService productService, 
            IBrandService brandService, 
            ICartDetailService cartDetailService, 
            ICategoryService categoryService, 
            IOrderService orderService, 
            IOrderDetailService orderDetailService, 
            IUserService userService)
        {
            _productService = productService;
            _brandService = brandService;
            _cartDetailService = cartDetailService;
            _categoryService = categoryService;
            _orderService = orderService;
            _orderDetailService = orderDetailService;
            _userService = userService;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
