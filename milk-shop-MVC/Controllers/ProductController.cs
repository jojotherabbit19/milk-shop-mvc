﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;

namespace milk_shop_MVC.Controllers
{
    public class ProductController : BaseController
    {
        public ProductController(IProductService productService, IBrandService brandService, ICartDetailService cartDetailService, ICategoryService categoryService, IOrderService orderService, IOrderDetailService orderDetailService, IUserService userService) : base(productService, brandService, cartDetailService, categoryService, orderService, orderDetailService, userService)
        {
        }

        public async Task<IActionResult> Index()
        {
            var result = await _productService.GetAllProduct();
            return View(result);
        }
        public async Task<IActionResult> ProductDetail([FromRoute] int id)
        {
            var result = await _productService.GetProductById(id);
            return View(result);
        }
        public async Task<IActionResult> ProductBrand([FromRoute] int id)
        {
            var result = await _productService.GetByBrand(id);
            return View(result);
        }
        public async Task<IActionResult> ProductCategory([FromRoute] int id)
        {
            var result = await _productService.GetByCategory(id);
            return View(result);
        }
    }
}
