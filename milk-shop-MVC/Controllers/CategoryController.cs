﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;

namespace milk_shop_MVC.Controllers
{
    public class CategoryController : BaseController
    {
        public CategoryController(IProductService productService, IBrandService brandService, ICartDetailService cartDetailService, ICategoryService categoryService, IOrderService orderService, IOrderDetailService orderDetailService, IUserService userService) : base(productService, brandService, cartDetailService, categoryService, orderService, orderDetailService, userService)
        {
        }
    }
}
