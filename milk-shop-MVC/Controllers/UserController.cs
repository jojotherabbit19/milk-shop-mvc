﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;

namespace milk_shop_MVC.Controllers
{
    public class UserController : BaseController
    {
        public UserController(IProductService productService, IBrandService brandService, ICartDetailService cartDetailService, ICategoryService categoryService, IOrderService orderService, IOrderDetailService orderDetailService, IUserService userService) : base(productService, brandService, cartDetailService, categoryService, orderService, orderDetailService, userService)
        {
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(string username, string password)
        {
            try
            {
                var result = await _userService.Login(username, password);
                HttpContext.Session.SetString("username", username);
                HttpContext.Session.SetString("userId", result.Id.ToString());
                HttpContext.Session.SetString("password", result.Role.ToString());
                return RedirectToAction("Index", "Product");
            }
            catch (Exception)
            {

                return RedirectToAction("Error", "BaseController");
            }
        }
    }
}
