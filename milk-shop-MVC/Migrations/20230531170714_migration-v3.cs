﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace milk_shop_MVC.Migrations
{
    /// <inheritdoc />
    public partial class migrationv3 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Total",
                table: "Carts");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Total",
                table: "Carts",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
