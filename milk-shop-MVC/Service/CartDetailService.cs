﻿using AppService.IService;

namespace milk_shop_MVC.Service
{
    public class CartDetailService : ICartDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CartDetailService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<Cart> CreateCartProduct(Cart product)
        {
            await _unitOfWork.CartRepository.CreateAsync(product);
            await _unitOfWork.SaveChangeAsync();
            return product;
        }

        public async Task<bool> DeleteCartProduct(int id)
        {
            var result = await _unitOfWork.CartRepository.GetAsync(id);
            if (result is null)
            {
                throw new Exception($"Not found {id}");
            }
            _unitOfWork.CartRepository.DeleteAsync(result);
            await _unitOfWork.SaveChangeAsync();
            return true;
        }

        public async Task<List<Cart>> GetCart(int id)
        {
            return await _unitOfWork.CartRepository.GetCartDetail(id);
        }

        public Task<Cart> GetCartById(int id)
        {
            return _unitOfWork.CartRepository.GetAsync(id);
        }

        public async Task<Cart> UpdateCartProduct(Cart product)
        {
            _unitOfWork.CartRepository.UpdateAsync(product);
            await _unitOfWork.SaveChangeAsync();
            return product;
        }
    }
}
