﻿using AppService.IService;

namespace milk_shop_MVC.Service
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        public OrderDetailService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<OrderProduct> CreateOrderProduct(OrderProduct product)
        {
            await _unitOfWork.OrderDetailRepository.CreateAsync(product);
            await _unitOfWork.SaveChangeAsync();
            return product;
        }

        public async Task<bool> DeleteOrderProduct(int id)
        {
            var result = await _unitOfWork.OrderDetailRepository.GetAsync(id);
            if (result is null)
            {
                throw new Exception($"Not found {id}");
            }
            return true;
        }

        public async Task<OrderProduct> UpdateOrderProduct(OrderProduct product)
        {
            _unitOfWork.OrderDetailRepository.UpdateAsync(product);
            await _unitOfWork.SaveChangeAsync();
            return product;
        }
    }
}
