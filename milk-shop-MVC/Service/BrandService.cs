﻿using AppService.IService;

namespace milk_shop_MVC.Service
{
    public class BrandService : IBrandService
    {
        private readonly IUnitOfWork _unitOfWork;
        public BrandService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<Brand> CreateBrand(Brand brand)
        {
            await _unitOfWork.BrandRepository.CreateAsync(brand);
            await _unitOfWork.SaveChangeAsync();
            return brand;
        }

        public async Task<bool> DeleteBrand(int id)
        {
            var result = await _unitOfWork.BrandRepository.GetAsync(id);
            if (result is null)
            {
                throw new Exception("Not found");
            }
            _unitOfWork.BrandRepository.DeleteAsync(result);
            await _unitOfWork.SaveChangeAsync();
            return true;
        }

        public async Task<List<Brand>> GetBrand()
        {
            return await _unitOfWork.BrandRepository.GetAllAsync();
        }

        public async Task<Brand> GetBrandById(int id)
        {
            var result = await _unitOfWork.BrandRepository.GetAsync(id);
            if (result is null)
            {
                throw new Exception("Not found");
            }
            return result;
        }

        public async Task<Brand> UpdateBrand(Brand brand)
        {
            _unitOfWork.BrandRepository.UpdateAsync(brand);
            await _unitOfWork.SaveChangeAsync();
            return brand;
        }
    }
}
