﻿public class ProductService : IProductService
{
    private readonly IUnitOfWork _unitOfWork;
    public ProductService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<bool> AddProduct(Product product)
    {
        await _unitOfWork.ProductRepository.CreateAsync(product);
        var isSuccess = await _unitOfWork.SaveChangeAsync();
        if (isSuccess < 0)
        {
            throw new Exception("Add Fail");
        }
        return true;
    }

    public async Task<bool> DeleteProduct(int id)
    {
        var pro = await _unitOfWork.ProductRepository.GetAsync(id);
        if (pro is null)
        {
            throw new Exception($"Product {id} does not exist");
        }
        _unitOfWork.ProductRepository.DeleteAsync(pro);
        var isSuccess = await _unitOfWork.SaveChangeAsync();
        if (isSuccess < 0)
        {
            throw new Exception("Delete Fail");
        }
        return true;
    }

    public async Task<List<Product>?> GetAllProduct()
    {
        var lstProduct = await _unitOfWork.ProductRepository.GetAllAsync();
        if (lstProduct is null)
        {
            return null;
        }
        return lstProduct;
    }

    public async Task<List<Product>> GetByBrand(int id)
    {
        return await _unitOfWork.ProductRepository.GetProductByBrand(id);
    }

    public async Task<List<Product>> GetByCategory(int id)
    {
        return await _unitOfWork.ProductRepository.GetProductByCategory(id);
    }

    public async Task<Product> GetProductById(int id)
    {
        var product = await _unitOfWork.ProductRepository.GetDetail(id);
        if (product is null)
        {
            throw new Exception($"Product {id} does not exist");
        }
        return product;
    }

    public async Task<List<Product>> GetTop6()
    {
        return await _unitOfWork.ProductRepository.GetTop6ByAsc();
    }

    public async Task<bool> UpdateProduct(Product product)
    {
        _unitOfWork.ProductRepository.DeleteAsync(product);
        var isSuccess = await _unitOfWork.SaveChangeAsync();
        if (isSuccess < 0)
        {
            throw new Exception("Update Fail");
        }
        return true;
    }
}