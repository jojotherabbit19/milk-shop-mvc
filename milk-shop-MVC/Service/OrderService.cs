﻿using AppService.IService;

namespace milk_shop_MVC.Service
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        public OrderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<Order> CreateOrder(Order order)
        {
            await _unitOfWork.OrderRepository.CreateAsync(order);
            await _unitOfWork.SaveChangeAsync();
            return order;
        }

        public async Task<bool> DeleteOrder(int id)
        {
            var result = await _unitOfWork.OrderRepository.GetAsync(id);
            if (result is null)
            {
                throw new Exception($"Not found {id}");
            }
            return true;
        }

        public async Task<List<Order>> GetAllOrders()
        {
            var result = await _unitOfWork.OrderRepository.GetOrderDetailList();
            if (result is null)
            {
                return new List<Order>();
            }
            return result;
        }

        public async Task<Order> GetOrder(int id)
        {
            var result = await _unitOfWork.OrderRepository.GetOrderDetail(id);
            if (result is null)
            {
                throw new Exception($"Not found {id}");
            }
            return result;
        }

        public async Task<Order> UpdateOrder(Order order)
        {
            _unitOfWork.OrderRepository.UpdateAsync(order);
            await _unitOfWork.SaveChangeAsync();
            return order;
        }
    }
}
