﻿using AppService.IService;

namespace milk_shop_MVC.Service
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<User> CreateAsync(User user)
        {
            await _unitOfWork.UserRepository.CreateAsync(user);
            await _unitOfWork.SaveChangeAsync();
            return user;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var result = await _unitOfWork.UserRepository.GetAsync(id);
            if (result is null)
            {
                throw new Exception($"Not found {id}");
            }
            _unitOfWork.UserRepository.DeleteAsync(result);
            await _unitOfWork.SaveChangeAsync();
            return true;
        }

        public async Task<User> GetByIdAsync(int id)
        {
            var result = await _unitOfWork.UserRepository.GetAsync(id);
            if (result is null)
            {
                throw new Exception($"Not found {id}");
            }
            return result;
        }

        public async Task<User> Login(string username, string password)
        {
            var result = await _unitOfWork.UserRepository.Login(username, password);
            if (result is null)
            {
                throw new Exception("Not found user");
            }
            return result;
        }

        public async Task<User> UpdateAsync(User user)
        {
            _unitOfWork.UserRepository.UpdateAsync(user);
            await _unitOfWork.SaveChangeAsync();
            return user;
        }
    }
}
