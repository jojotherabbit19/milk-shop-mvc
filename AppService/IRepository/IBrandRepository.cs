﻿namespace AppService.IRepository
{
    public interface IBrandRepository : IGenericRepository<Brand>
    {
    }
}
