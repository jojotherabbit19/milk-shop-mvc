﻿namespace AppService.IRepository
{
    public interface IOrderDetailRepository : IGenericRepository<OrderProduct>
    {
    }
}
