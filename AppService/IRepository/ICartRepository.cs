﻿namespace AppService.IRepository
{
    public interface ICartRepository : IGenericRepository<Cart>
    {
        Task<List<Cart>> GetCartDetail(int id);
    }
}
