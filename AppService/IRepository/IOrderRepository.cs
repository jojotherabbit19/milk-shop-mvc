﻿namespace AppService.IRepository
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
        Task<List<Order>?> GetOrderDetailList();
        Task<Order?> GetOrderDetail(int id);
    }
}
