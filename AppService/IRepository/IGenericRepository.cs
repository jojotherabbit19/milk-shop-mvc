﻿public interface IGenericRepository<T>
{
    Task CreateAsync(T entity);
    void UpdateAsync(T entity);
    void DeleteAsync(T entity);
    Task<T?> GetAsync(int? id);
    Task<List<T>?> GetAllAsync();
}