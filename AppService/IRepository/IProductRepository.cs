﻿public interface IProductRepository : IGenericRepository<Product>
{
    Task<List<Product>> GetProductByCategory(int id);
    Task<List<Product>> GetProductByBrand(int id);
    Task<List<Product>> GetTop6ByAsc();
    Task<Product> GetDetail(int id);
}