﻿namespace AppService.IRepository
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
    }
}
