﻿namespace AppService.IRepository
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<bool> IsExist(string username);
        Task<User> Login(string username, string password);
    }
}
