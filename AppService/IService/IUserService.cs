﻿namespace AppService.IService
{
    public interface IUserService
    {
        Task<User> CreateAsync(User user);
        Task<User> UpdateAsync(User user);
        Task<bool> DeleteAsync(int id);
        Task<User> GetByIdAsync(int id);
        Task<User> Login(string username, string password);
    }
}
