﻿namespace AppService.IService
{
    public interface ICartDetailService
    {
        Task<List<Cart>> GetCart(int id);
        Task<Cart> GetCartById(int id);
        Task<Cart> CreateCartProduct(Cart product);
        Task<Cart> UpdateCartProduct(Cart product);
        Task<bool> DeleteCartProduct(int id);
    }
}
