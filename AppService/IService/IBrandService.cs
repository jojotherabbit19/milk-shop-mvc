﻿namespace AppService.IService
{
    public interface IBrandService
    {
        Task<List<Brand>> GetBrand();
        Task<Brand> GetBrandById(int id);
        Task<Brand> CreateBrand(Brand brand);
        Task<Brand> UpdateBrand(Brand brand);
        Task<bool> DeleteBrand(int id);
    }
}
