﻿namespace AppService.IService
{
    public interface IOrderDetailService
    {
        Task<OrderProduct> CreateOrderProduct(OrderProduct product);
        Task<OrderProduct> UpdateOrderProduct(OrderProduct product);
        Task<bool> DeleteOrderProduct(int id);
    }
}
