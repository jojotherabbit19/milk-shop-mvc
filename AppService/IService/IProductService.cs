﻿public interface IProductService
{
    Task<List<Product>?> GetAllProduct();
    Task<Product> GetProductById(int id);
    Task<bool> AddProduct(Product product);
    Task<bool> UpdateProduct(Product product);
    Task<bool> DeleteProduct(int id);
    Task<List<Product>> GetByBrand(int id);
    Task<List<Product>> GetByCategory(int id);
    Task<List<Product>> GetTop6();
}