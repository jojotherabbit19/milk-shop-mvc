﻿namespace AppService.IService
{
    public interface ICategoryService
    {
        Task<List<Category>> GetCategory();
        Task<Category> GetCategory(int id);
        Task<Category> CreateCategory(Category category);
        Task<Category> UpdateCategory(Category category);
        Task<bool> DeleteCategory(int id);
    }
}
