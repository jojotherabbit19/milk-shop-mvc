﻿namespace AppService.IService
{
    public interface IOrderService
    {
        Task<Order> CreateOrder(Order order);
        Task<Order> UpdateOrder(Order order);
        Task<bool> DeleteOrder(int id);
        Task<Order> GetOrder(int id);
        Task<List<Order>> GetAllOrders();
    }
}
