﻿public class Product : BaseEntity
{
    public string ProductName { get; set; }
    public decimal Price { get; set; }
    public string ImageUrl { get; set; }
    public int? ImportBy { get; set; }
    public DateTime ImportDate { get; set; }
    public string? Description { get; set; }
    public int Quantity { get; set; }
    public int BrandId { get; set; }
    public Brand Brand { get; set; }
    public int CategoryId { get; set; }
    public Category Category { get; set; }
    public ICollection<Cart>? Carts { get; set; }
    public ICollection<OrderProduct>? OrderProducts { get; set; }
}