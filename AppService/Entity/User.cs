﻿public class User : BaseEntity
{
    public string UserName { get; set; }
    public string PasswordHash { get; set; }
    public string FullName { get; set; }
    public string Phone { get; set; }
    public string Address { get; set; }
    public RoleEnum Role { get; set; }
    public ICollection<Order>? Orders { get; set; }
    public ICollection<Cart>? Carts { get; set; }
}