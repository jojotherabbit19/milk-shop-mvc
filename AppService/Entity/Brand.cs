﻿public class Brand : BaseEntity
{
    public string BrandName { get; set; }
    public string Hotline { get; set; }
    public string? Description { get; set; }
}