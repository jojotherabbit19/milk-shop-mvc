﻿public class Order : BaseEntity
{
    public int UserId { get; set; }
    public User User { get; set; }
    public DateTime OrderDate { get; set; }
    public decimal Total { get; set; }
    public ICollection<OrderProduct>? OrderProducts { get; set; }
}