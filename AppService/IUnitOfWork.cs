﻿using AppService.IRepository;

public interface IUnitOfWork
{
    public IProductRepository ProductRepository { get; }
    public ICartRepository CartRepository { get; }
    public IOrderRepository OrderRepository { get; }
    public IOrderDetailRepository OrderDetailRepository { get; }
    public IUserRepository UserRepository { get; }
    public ICategoryRepository CategoryRepository { get; }
    public IBrandRepository BrandRepository { get; }
    public Task<int> SaveChangeAsync();
}